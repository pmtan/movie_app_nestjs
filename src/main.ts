import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { PrismaService } from './prisma.service';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);
  app.enableCors();
  app.use(express.static('.'));
  const config = new DocumentBuilder()
    .setTitle('Movie App')
    .setDescription('Movie API')
    .setVersion('1.0')
    .addTag(
      'Account Login:',
      `{
      "email": "turtle@gmail.com",
      "mat_khau": "123456"
    }`,
    )
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(8080);
}
bootstrap();

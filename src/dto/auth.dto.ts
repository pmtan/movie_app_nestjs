import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  mat_khau: string;
}

export class SignUpDto {
    @ApiProperty()
    ho_ten: string;

    @ApiProperty()
    so_dt: string;

    @ApiProperty()
    email: string;
  
    @ApiProperty()
    mat_khau: string;
  }
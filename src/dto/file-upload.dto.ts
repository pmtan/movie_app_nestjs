import { ApiProperty } from '@nestjs/swagger';

export class PhotoUploadDTO {
  @ApiProperty({ type: 'string', format: 'binary' })
  fileUpload: Express.Multer.File;
}

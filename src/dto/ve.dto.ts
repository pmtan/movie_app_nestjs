import { ApiProperty } from '@nestjs/swagger';

class GheDTO {
  @ApiProperty()
  ma_ghe: number;
}
export class DatVeDTO {
  @ApiProperty()
  tai_khoan: number;
  @ApiProperty()
  ma_lich_chieu: number;
  @ApiProperty({ type: [GheDTO] })
  danh_sach_ghe: GheDTO[];
}

export class LichChieuUploadDTO {
  @ApiProperty({ type: 'number' })
  ma_rap: number;
  @ApiProperty({ type: 'number' })
  ma_phim: number;
  @ApiProperty()
  ngay_gio_chieu: Date;
}

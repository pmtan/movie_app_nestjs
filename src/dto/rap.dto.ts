export enum EnumHeThongRap {
  BHDStar = 'BHD Star Cineplex',
  CGV = 'CGV',
  CineStar = 'Cine Star',
  Galaxy = 'Galaxy Cinema',
  LotteCinema = 'Lotte Cinema',
  MegaGS = 'MegaGS Cinema',
}

export enum EnumMaHeThongRap {
  BHDStar = 'BHDStar',
  CGV = 'CGV',
  CineStar = 'CineStar',
  Galaxy = 'Galaxy',
  LotteCinema = 'LotteCinema',
  MegaGS = 'MegaGS',
}

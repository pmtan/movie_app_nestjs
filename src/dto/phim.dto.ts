import { ApiProperty } from '@nestjs/swagger';
import { Decimal } from '@prisma/client/runtime';

export interface BannerDTO {
  ma_banner: number;
  ma_phim: number;
  hinh_anh: string;
}

export interface PhimDTO {
  ma_phim: number;
  ten_phim: string;
  trailer: string;
  hinh_anh: string;
  mo_ta: string;
  ngay_khoi_chieu: Date;
  danh_gia: Decimal;
  hot: boolean;
  dang_chieu: boolean;
  sap_chieu: boolean;
  Banner?: BannerDTO[];
  LichChieu?: Date[];
}

export class MovieDetailUploadDTO {
  @ApiProperty()
  ten_phim: string;

  @ApiProperty()
  trailer: string;

  @ApiProperty()
  mo_ta: string;

  @ApiProperty()
  ngay_khoi_chieu: Date;

  @ApiProperty()
  danh_gia: number;

  @ApiProperty()
  hot: boolean;

  @ApiProperty()
  dang_chieu: boolean;

  @ApiProperty()
  sap_chieu: boolean;
}

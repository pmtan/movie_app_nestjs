import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LichChieu, Phim } from '@prisma/client';
import { PhimService } from './phim.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { PhotoUploadDTO } from 'src/dto/file-upload.dto';
import { BannerDTO, MovieDetailUploadDTO, PhimDTO } from 'src/dto/phim.dto';

@ApiBearerAuth()
@ApiTags('QuanLyPhim')
@UseGuards(AuthGuard('jwt'))
@Controller('QuanLyPhim')
export class PhimController {
  constructor(private phimService: PhimService) {}

  @Get('/LayDanhSachBanner')
  getBanners(): Promise<BannerDTO[]> {
    return this.phimService.getBanners();
  }

  @Get('/LayDanhSachPhim')
  getMovies(): Promise<PhimDTO[]> {
    return this.phimService.getMovies();
  }

  @Get('/LayThongTinPhim')
  getMovieDetail(@Query('movieId') movieId: number): Promise<Phim> {
    return this.phimService.getMovieDetail(movieId);
  }

  @Get('/LayThongTinPhimPhanTrang')
  getMoviesByPages(
    @Query('page') page: number,
    @Query('quantity') quantity: number,
  ): Promise<Phim[]> {
    return this.phimService.getMoviesByPages(page, quantity);
  }

  @Get('/LayThongTinPhimTheoNgay')
  getMoviesByDates(
    @Query('begin') begin: Date,
    @Query('end') end: Date,
  ): Promise<LichChieu[]> {
    return this.phimService.getMoviesByDates(new Date(begin), new Date(end));
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: PhotoUploadDTO,
  })
  @UseInterceptors(
    FileInterceptor('fileUpload', {
      storage: diskStorage({
        destination: process.cwd() + '/public/movieImg',
        filename: (req, file, cb) => cb(null, Date.now() + file.originalname),
      }),
    }),
  )
  @Post('ThemPhimUploadHinh')
  uploadMoviePhoto(
    @UploadedFile() file: Express.Multer.File,
    @Query('movieId') movieId: number,
  ): Promise<string> {
    return this.phimService.uploadMoviePhoto(movieId, file.filename);
  }

  @ApiBody({
    type: MovieDetailUploadDTO,
  })
  @Post('/CapNhatThongTinPhim')
  updateMovieInfo(
    @Body() body: MovieDetailUploadDTO,
    @Query('ma_phim') movieId: number,
  ): Promise<Phim> {
    return this.phimService.updateMovie(movieId, body);
  }

  @ApiBody({
    type: MovieDetailUploadDTO,
  })
  @Post('/ThemPhimUploadThongTinPhim')
  uploadMovie(@Body() body: MovieDetailUploadDTO): Promise<string> {
    return this.phimService.uploadMovie(body);
  }
  @Delete('/XoaPhim')
  deleteMovie(@Query('maPhim') movieId: number): Promise<string> {
    return this.phimService.deleteMovie(movieId);
  }
}

import { HttpException, Injectable } from '@nestjs/common';
import { Phim, PrismaClient, LichChieu } from '@prisma/client';
import { BannerDTO, MovieDetailUploadDTO, PhimDTO } from 'src/dto/phim.dto';
@Injectable()
export class PhimService {
  private prisma: PrismaClient = new PrismaClient();
  async getBanners(): Promise<BannerDTO[]> {
    try {
      let data = await this.prisma.banner.findMany();
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Lỗi lấy danh sách banner.', 400);
      }
    } catch (error) {
      console.log(error);
      throw new HttpException('Lỗi lấy danh sách banner.', 400);
    }
  }
  async getMovies(): Promise<PhimDTO[]> {
    try {
      let data = await this.prisma.phim.findMany();
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Lỗi lấy danh sách phim.', 400);
      }
    } catch (error) {
      throw new HttpException('Lỗi lấy danh sách phim.', 400);
    }
  }
  async getMoviesByPages(page: number, quantity: number): Promise<Phim[]> {
    try {
      let data = await this.prisma.phim.findMany({
        skip: Number(quantity) * (page - 1),
        take: Number(quantity),
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Lỗi lấy danh sách phim phân trang.', 400);
      }
    } catch (error) {
      console.log(error);
      throw new HttpException('Lỗi lấy danh sách phim phân trang.', 400);
    }
  }
  async getMoviesByDates(begin: Date, end: Date): Promise<LichChieu[]> {
    return await this.prisma.lichChieu.findMany({
      where: {
        ngay_gio_chieu: {
          gte: begin,
          lte: end,
        },
      },
      include: {
        Phim: true,
        RapPhim: true,
      },
    });
  }
  async uploadMoviePhoto(movieId: number, moviePhoto: string): Promise<string> {
    try {
      await this.prisma.phim.update({
        where: {
          ma_phim: Number(movieId),
        },
        data: {
          hinh_anh: moviePhoto,
        },
      });
      return 'Upload hình phim thành công!.';
    } catch (err) {
      console.log(err);
      return 'Lỗi upload hình phim.';
    }
  }
  async updateMovie(
    movieId: number,
    body: MovieDetailUploadDTO,
  ): Promise<Phim> {
    try {
      let {
        ten_phim,
        trailer,
        mo_ta,
        ngay_khoi_chieu,
        danh_gia,
        hot,
        dang_chieu,
        sap_chieu,
      } = body;
      let res = await this.prisma.phim.update({
        where: {
          ma_phim: Number(movieId),
        },
        data: {
          ten_phim: ten_phim || undefined,
          trailer: trailer || undefined,
          mo_ta: mo_ta || undefined,
          ngay_khoi_chieu: ngay_khoi_chieu || undefined,
          danh_gia: danh_gia || undefined,
          hot: hot || undefined,
          dang_chieu: dang_chieu || undefined,
          sap_chieu: sap_chieu || undefined,
        },
      });
      console.log(res);
      if (res) {
        return res;
      } else {
        throw new HttpException('Lỗi cập nhật thông tin phim.', 400);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Lỗi cập nhật thông tin phim.', 400);
    }
  }
  async uploadMovie(body: MovieDetailUploadDTO): Promise<string> {
    try {
      let {
        ten_phim,
        trailer,
        mo_ta,
        ngay_khoi_chieu,
        danh_gia,
        hot,
        dang_chieu,
        sap_chieu,
      } = body;
      let res = await this.prisma.phim.create({
        data: {
          ten_phim,
          trailer,
          mo_ta,
          ngay_khoi_chieu,
          danh_gia,
          hot,
          dang_chieu,
          sap_chieu,
        },
      });
      if (res) {
        return 'Thêm phim mới thành công!';
      } else {
        throw new HttpException('Lỗi thêm phim mới.', 400);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Lỗi thêm phim mới.', 400);
    }
  }
  async getMovieDetail(movieId: number): Promise<Phim> {
    try {
      let res = await this.prisma.phim.findFirst({
        where: {
          ma_phim: Number(movieId),
        },
      });
      if (res) {
        return res;
      } else {
        throw new HttpException('Không tìm thấy thông tin phim.', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy thông tin phim.', 404);
    }
  }
  async deleteMovie(movieId: number): Promise<string> {
    try {
      let res = await this.prisma.phim.delete({
        where: {
          ma_phim: Number(movieId),
        },
      });
      if (res) {
        return 'Xoá phim thành công!';
      } else {
        throw new HttpException('Lỗi xoá phim BE.', 400);
      }
    } catch (error) {
      console.log(error);
      throw new HttpException('Lỗi xoá phim BE.', 400);
    }
  }
}

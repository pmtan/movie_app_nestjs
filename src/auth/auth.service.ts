import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from 'src/prisma.service';
import * as bcrypt from 'bcrypt';
import { NguoiDung } from '@prisma/client';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly prisma:PrismaService,
        private readonly configService:ConfigService
    ){}
    async login(email: string, mat_khau:string): Promise<string> {
        try {
            const user = await this.prisma.nguoiDung.findFirst({
                where:{
                    email
                }
            })
            if (user) {
                const checkPassword = bcrypt.compareSync(mat_khau, user.mat_khau);
                if (checkPassword){
                    let token = this.jwtService.sign({...user, mat_khau:""},{ secret: this.configService.get("JWT_SECRET"), expiresIn: "30m" })
                    return token;
                } else {
                    return "Sai mat khau."
                }
            } else {
                return "Email khong ton tai."
            }
            
        }
        catch(err) {
            console.log(err);
        }
    }
    async signup(ho_ten:string, so_dt: string, email: string, mat_khau:string): Promise<string> {
        try {
            let checkEmail = this.prisma.nguoiDung.findFirst({
                where: {
                    email
                }
            })
            if (checkEmail){
                return "Email đã tồn tại."
            } else {
                let nguoiDungModel = {ho_ten, so_dt, email, mat_khau: bcrypt.hashSync(mat_khau,10), loai_nguoi_dung: "user"}
                await this.prisma.nguoiDung.create({data: nguoiDungModel})
                return "Tao nguoi dung thanh cong!"
            }
        }
        catch(err) {
            console.log(err);
            return "Loi BE."
        }
    }
    async getToken(email: string, mat_khau:string): Promise<string> {
        try {
            const user = await this.prisma.nguoiDung.findFirst({
                where:{
                    email
                }
            })
            if (user) {
                const checkPassword = bcrypt.compareSync(mat_khau, user.mat_khau);
                if (checkPassword){
                    let token = this.jwtService.sign({...user, mat_khau:""},{ secret: this.configService.get("ROLE_SECRET"), expiresIn: "30m" })
                    return token;
                } else {
                    return "Sai mat khau."
                }
            } else {
                return "Email khong ton tai."
            }
            
        }
        catch(err) {
            console.log(err);
        }
    }
}

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LoginDto, SignUpDto } from 'src/dto/auth.dto';
import { AuthService } from './auth.service';


@ApiTags("Authentication")
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService){};

    @HttpCode(HttpStatus.OK)
    @Post("login")
    login(@Body() body: LoginDto): Promise<string> {
        let {email, mat_khau} = body;
         return this.authService.login(email,mat_khau)
    }

    @HttpCode(HttpStatus.OK)
    @Post("signup")
    signup(@Body() body: SignUpDto): Promise<string> {
        let {ho_ten, so_dt, email, mat_khau} = body;
        return this.authService.signup(ho_ten, so_dt, email, mat_khau);
    }
}

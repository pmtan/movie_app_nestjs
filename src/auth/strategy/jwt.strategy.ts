import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy} from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        config: ConfigService,
        ) {
        super({
            jwtFromRequest:
                ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.get("JWT_SECRET"),
        });
    }
    // async checkToken(@Headers("token") token:string):Promise<Boolean>{
    //     return this.jwtService.verifyAsync(token);
    // }
    async validate(token: string) {
        // check ADMIN, USER
        // console.log("validate")
        return token
    }
    //     // throw new HttpException("không có quyền", 401);
    //     // thông tin giải mã của token khi xác minh thành công
    //     return token;
    // }
}
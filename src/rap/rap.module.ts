import { Module } from '@nestjs/common';
import { JwtStrategy } from 'src/auth/strategy/jwt.strategy';
import { PrismaService } from 'src/prisma.service';
import { RapController } from './rap.controller';
import { RapService } from './rap.service';

@Module({
  controllers: [RapController],
  providers: [RapService,PrismaService, JwtStrategy]
})
export class RapModule {}

import { HttpException, Injectable } from '@nestjs/common';
import { CumRap, HeThongRap, LichChieu, RapPhim } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class RapService {
  constructor(private readonly prisma: PrismaService) {}

  async getTheaterSystem(ten_he_thong_rap: string): Promise<HeThongRap> {
    try {
      const data = this.prisma.heThongRap.findFirst({
        where: {
          ten_he_thong_rap,
        },
      });
      if (data) {
        return data;
      } else {
        throw new HttpException('Không tìm thấy thông tin hệ thống rạp.', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy thông tin hệ thống rạp.', 404);
    }
  }
  async getTheaters(ma_he_thong_rap: string): Promise<CumRap[]> {
    try {
      const data = await this.prisma.cumRap.findMany({
        where: {
          ma_he_thong_rap,
        },
        include: {
          RapPhim: true,
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException(
          'Không tìm thấy thông tin cụm rạp theo hệ thống',
          400,
        );
      }
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Không tìm thấy thông tin cụm rạp theo hệ thống',
        400,
      );
    }
  }
  async getShowtimeByTheaters(ma_he_thong_rap: string): Promise<CumRap[]> {
    try {
      const data = await this.prisma.cumRap.findMany({
        where: {
          ma_he_thong_rap,
        },
        include: {
          RapPhim: {
            include: {
              LichChieu: {
                include: {
                  Phim: true,
                },
              },
            },
          },
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException(
          'Lỗi lấy thông tin lịch chiếu theo hệ thống.',
          404,
        );
      }
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Lỗi lấy thông tin lịch chiếu theo hệ thống.',
        404,
      );
    }
  }
  async getShowtimeByMovie(ma_phim: number): Promise<any> {
    try {
      let data = await this.prisma.phim.findMany({
        where: {
          ma_phim: Number(ma_phim),
        },
        include: {
          LichChieu: {
            select: {
              RapPhim: {
                select: {
                  CumRap: {
                    select: {
                      ten_cum_rap: true,
                      dia_chi: true,
                    },
                  },
                  ten_rap: true,
                },
              },
              ngay_gio_chieu: true,
            },
          },
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Không tìm thấy mã phim.', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy mã phim.', 404, {
        cause: new Error(err),
      });
    }
  }
}

import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { HeThongRap, LichChieu, Phim } from '@prisma/client';
import { RapService } from './rap.service';
import { EnumHeThongRap, EnumMaHeThongRap } from 'src/dto/rap.dto';

@ApiBearerAuth()
@ApiTags('QuanLyRap')
@UseGuards(AuthGuard('jwt'))
@Controller('QuanLyRap')
export class RapController {
  constructor(private readonly rapService: RapService) {}

  @ApiQuery({ name: 'ten_he_thong_rap', enum: EnumHeThongRap })
  @Get('LayThongTinHeThongRap')
  getTheaterSystem(
    @Query('ten_he_thong_rap') ten_he_thong_rap: string,
  ): Promise<HeThongRap> {
    return this.rapService.getTheaterSystem(ten_he_thong_rap);
  }

  @ApiQuery({ name: 'ma_he_thong_rap', enum: EnumMaHeThongRap })
  @Get('/LayThongTinCumRapTheoHeThong')
  getTheaters(@Query('ma_he_thong_rap') ma_he_thong_rap: string): Promise<any> {
    return this.rapService.getTheaters(ma_he_thong_rap);
  }

  @ApiQuery({ name: 'ma_he_thong_rap', enum: EnumMaHeThongRap })
  @Get('/LayThongTinLichChieuHeThongRap')
  getShowtimeByTheaters(
    @Query('ma_he_thong_rap') ma_he_thong_rap: string,
  ): Promise<any> {
    return this.rapService.getShowtimeByTheaters(ma_he_thong_rap);
  }
  @Get('/LayThongTinLichChieuPhim')
  getShowtimeByMovie(@Query('ma_phim') ma_phim: number): Promise<any> {
    return this.rapService.getShowtimeByMovie(ma_phim);
  }
}

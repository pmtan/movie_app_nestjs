import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { VeService } from './ve.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { DatVeDTO, LichChieuUploadDTO } from 'src/dto/ve.dto';
import { LichChieu } from '@prisma/client';

// @ApiBearerAuth()
@ApiTags('QuanLyDatVe')
// @UseGuards(AuthGuard('jwt'))
@Controller('QuanLyDatVe')
export class VeController {
  constructor(private readonly veService: VeService) {}
  @ApiBody({ type: DatVeDTO })
  @Post('DatVe')
  bookTicket(@Body() body: DatVeDTO): Promise<string> {
    return this.veService.bookTicket(body);
  }

  @Get('LayDanhSachPhongVe')
  getTheaterInfo(
    @Query('maLichChieu') maLichChieu: number,
  ): Promise<LichChieu> {
    return this.veService.getTheaterInfo(maLichChieu);
  }

  @ApiBody({ type: LichChieuUploadDTO })
  @Post('TaoLichChieu')
  createShowtime(@Body() body: LichChieuUploadDTO): Promise<string> {
    return this.veService.createShowtime(body);
  }

  @Get('LayTatCaLichChieu')
  getAllShowtimes(@Query('maPhim') maPhim: number): Promise<LichChieu[]> {
    return this.veService.getAllShowtimes(maPhim);
  }
}

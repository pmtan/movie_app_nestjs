import { HttpException, Injectable } from '@nestjs/common';
import { LichChieu } from '@prisma/client';
import { DatVeDTO, LichChieuUploadDTO } from 'src/dto/ve.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class VeService {
  constructor(private readonly prismaService: PrismaService) {}

  async bookTicket(body: DatVeDTO): Promise<string> {
    try {
      let { tai_khoan, ma_lich_chieu, danh_sach_ghe } = body;
      let listGhe = [];
      let listMaGhe = [];
      if (danh_sach_ghe.length > 0) {
        danh_sach_ghe.map((ghe) => {
          listGhe.push({
            tai_khoan: Number(tai_khoan),
            ma_lich_chieu: Number(ma_lich_chieu),
            ma_ghe: Number(ghe.ma_ghe),
          });
          listMaGhe.push(Number(ghe.ma_ghe));
        });
      } else {
        return;
      }
      let dataDatVe = await this.prismaService.datVe.createMany({
        data: listGhe,
      });

      let dataGhe = await this.prismaService.ghe.updateMany({
        where: {
          ma_ghe: {
            in: listMaGhe,
          },
        },
        data: {
          da_dat: true,
        },
      });
      if (dataDatVe && dataGhe) {
        return 'Đặt vé thành công!';
      } else {
        throw new HttpException('Lỗi đặt vé', 400);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Lỗi đặt vé', 400);
    }
  }

  async getTheaterInfo(maLichChieu: number): Promise<LichChieu> {
    try {
      let data = await this.prismaService.lichChieu.findFirst({
        where: {
          ma_lich_chieu: Number(maLichChieu),
        },
        include: {
          Phim: true,
          RapPhim: {
            select: {
              CumRap: {
                select: {
                  ten_cum_rap: true,
                  dia_chi: true,
                },
              },
              ten_rap: true,
              Ghe: {
                select: {
                  ten_ghe: true,
                  loai_ghe: true,
                  da_dat: true,
                },
              },
            },
          },
        },
      });
      if (data) {
        return data;
      } else {
        throw new HttpException('Lỗi lấy thông tin lịch chiếu.', 400);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Lỗi lấy thông tin lịch chiếu.', 400);
    }
  }

  async createShowtime(body: LichChieuUploadDTO): Promise<string> {
    try {
      let { ma_rap, ma_phim, ngay_gio_chieu } = body;
      let res = await this.prismaService.lichChieu.create({
        data: {
          ma_rap: Number(ma_rap),
          ma_phim: Number(ma_phim),
          ngay_gio_chieu,
        },
      });
      console.log(res);
      if (res) {
        return 'Tạo lịch chiếu mới thành công!';
      } else {
        throw new HttpException('Lỗi tạo lịch chiếu mới.', 400);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Lỗi tạo lịch chiếu mới.', 400);
    }
  }

  async getAllShowtimes(maPhim: number): Promise<LichChieu[]> {
    try {
      let data = await this.prismaService.lichChieu.findMany({
        where: {
          ma_phim: Number(maPhim),
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Lỗi lấy danh sách lịch chiếu theo phim.', 400);
      }
    } catch (error) {
      console.log(error);
      throw new HttpException('Lỗi lấy danh sách lịch chiếu theo phim.', 400);
    }
  }
}

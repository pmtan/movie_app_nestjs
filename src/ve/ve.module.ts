import { Module } from '@nestjs/common';
import { VeController } from './ve.controller';
import { VeService } from './ve.service';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from 'src/auth/strategy/jwt.strategy';

@Module({
  controllers: [VeController],
  providers: [VeService, PrismaService, JwtStrategy],
})
export class VeModule {}

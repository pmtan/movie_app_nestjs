import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PhimModule } from './phim/phim.module';
import { AuthModule } from './auth/auth.module';
import { NguoiDungModule } from './nguoi-dung/nguoi-dung.module';
import { RapModule } from './rap/rap.module';
import { VeModule } from './ve/ve.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    AuthModule,
    PhimModule,
    NguoiDungModule,
    RapModule,
    VeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

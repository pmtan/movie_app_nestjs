import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import { JwtStrategy } from 'src/auth/strategy/jwt.strategy';
import { NguoiDungController } from './nguoi-dung.controller';
import { NguoiDungService } from './nguoi-dung.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [NguoiDungController],
  providers: [NguoiDungService, PrismaService, JwtStrategy],
})
export class NguoiDungModule {}

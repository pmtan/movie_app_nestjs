import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { NguoiDung } from '@prisma/client';
import { NguoiDungService } from './nguoi-dung.service';
import { UserDTO } from 'src/dto/nguoi-dung.dto';

@ApiBearerAuth()
@ApiTags('QuanLyNguoiDung')
@UseGuards(AuthGuard('jwt'))
@Controller('QuanLyNguoiDung')
export class NguoiDungController {
  constructor(private readonly nguoiDungService: NguoiDungService) {}

  @Get('/LayDanhSachNguoiDung')
  getUsers(): Promise<NguoiDung[]> {
    return this.nguoiDungService.getUsers();
  }
  @Get('/LayDanhSachNguoiDungPhanTrang')
  getUsersByPage(
    @Query('page') page: number,
    @Query('usersPerPage') quantity: number,
  ): Promise<NguoiDung[]> {
    return this.nguoiDungService.getUsersByPage(page, quantity);
  }
  @Get('/TimKiemNguoiDung')
  findUser(@Query('keyword') keyword: string): Promise<NguoiDung[]> {
    return this.nguoiDungService.findUser(keyword);
  }
  @Get('/TimKiemNguoiDungPhanTrang')
  findUserPagination(
    @Query('keyword') keyword: string,
    @Query('page') page: number,
    @Query('userPerPage') quantity: number,
  ): Promise<NguoiDung[]> {
    return this.nguoiDungService.findUserPagination(keyword, page, quantity);
  }
  @Post('/LayThongTinNguoiDung')
  getUserInfo(@Query('userId') id: number): Promise<NguoiDung> {
    return this.nguoiDungService.getUserInfo(id);
  }
  @Post('/LayThongTinTaiKhoan')
  getAccountInfo(@Query('userId') id: number): Promise<NguoiDung> {
    return this.nguoiDungService.getAccountInfo(id);
  }

  @ApiBody({ type: UserDTO })
  @Post('/ThemNguoiDung')
  createUser(@Body() body: UserDTO): Promise<string> {
    return this.nguoiDungService.createUser(body);
  }

  @ApiBody({ type: UserDTO })
  @Put('/CapNhatThongTinNguoiDung')
  updateUser(@Body() body: UserDTO): Promise<string> {
    return this.nguoiDungService.updateUser(body);
  }
  @Delete('/XoaNguoiDung')
  deleteUser(@Query('taiKhoan') tai_khoan: number): Promise<string> {
    return this.nguoiDungService.deleteUser(tai_khoan);
  }
}

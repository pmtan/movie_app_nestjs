import { HttpException, Injectable } from '@nestjs/common';
import { NguoiDung } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class NguoiDungService {
  constructor(private readonly prisma: PrismaService) {}
  async getUsers(): Promise<NguoiDung[]> {
    const data = await this.prisma.nguoiDung.findMany();
    return data;
  }
  async getUserInfo(id: number): Promise<NguoiDung> {
    try {
      const userInfo = await this.prisma.nguoiDung.findFirst({
        where: {
          tai_khoan: Number(id),
        },
      });
      if (userInfo) {
        return userInfo;
      } else {
        throw new HttpException('Không tìm thấy người dùng!', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy người dùng!', 404);
    }
  }
  async getAccountInfo(id: number): Promise<NguoiDung> {
    try {
      return await this.prisma.nguoiDung.findFirst({
        where: {
          tai_khoan: Number(id),
        },
        include: {
          DatVe: true,
        },
      });
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy người dùng!', 404);
    }
  }
  async getUsersByPage(page: number, quantity: number): Promise<NguoiDung[]> {
    try {
      return await this.prisma.nguoiDung.findMany({
        skip: quantity * (page - 1),
        take: Number(quantity),
      });
    } catch (error) {
      console.log(error);
      throw new HttpException('Lỗi lấy người dùng phân trang', 400);
    }
  }
  async findUser(keyword: string): Promise<NguoiDung[]> {
    try {
      const data = await this.prisma.nguoiDung.findMany({
        where: {
          OR: [
            {
              ho_ten: {
                contains: keyword,
              },
            },
            {
              email: {
                contains: keyword,
              },
            },
            {
              so_dt: {
                contains: keyword,
              },
            },
          ],
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Không tìm thấy người dùng chứa từ khoá.', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy người dùng chứa từ khoá.', 404);
    }
  }
  async findUserPagination(
    keyword: string,
    page: number,
    quantity: number,
  ): Promise<NguoiDung[]> {
    try {
      let data = await this.prisma.nguoiDung.findMany({
        skip: quantity * (page - 1),
        take: Number(quantity),
        where: {
          OR: [
            {
              ho_ten: {
                contains: keyword,
              },
            },
            {
              email: {
                contains: keyword,
              },
            },
            {
              so_dt: {
                contains: keyword,
              },
            },
          ],
        },
      });
      if (data.length > 0) {
        return data;
      } else {
        throw new HttpException('Không tìm thấy người dùng chứa từ khoá.', 404);
      }
    } catch (err) {
      console.log(err);
      throw new HttpException('Không tìm thấy người dùng chứa từ khoá.', 404);
    }
  }
  async createUser(body: NguoiDung): Promise<string> {
    try {
      let { ho_ten, email, so_dt, mat_khau, loai_nguoi_dung } = body;
      await this.prisma.nguoiDung.create({
        data: {
          ho_ten,
          email,
          so_dt,
          mat_khau: bcrypt.hashSync(mat_khau, 10),
          loai_nguoi_dung,
        },
      });
      return 'Thêm người dùng mới thành công!';
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Lỗi thêm người dùng mới, vui lòng kiểm tra lại thông tin.',
        401,
      );
    }
  }
  async updateUser(body: NguoiDung): Promise<string> {
    try {
      let nguoiDungModel = {
        ...body,
        mat_khau: bcrypt.hashSync(body.mat_khau, 10),
      };
      await this.prisma.nguoiDung.update({
        where: {
          tai_khoan: body.tai_khoan,
        },
        data: nguoiDungModel,
      });
      return 'Cập nhật thông tin người dùng thành công!';
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Lỗi cập nhật thông tin người dùng. Vui lòng kiểm tra lại thông tin.',
        400,
        {
          cause: new Error(err),
        },
      );
    }
  }
  async deleteUser(tai_khoan: number): Promise<string> {
    try {
      let res = await this.prisma.nguoiDung.delete({
        where: {
          tai_khoan: Number(tai_khoan),
        },
      });
      if (res) {
        return 'Xoá người dùng thành công!';
      } else {
        throw new HttpException(
          'Lỗi xoá người dùng. Không tìm thấy người dùng.',
          404,
        );
      }
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Lỗi xoá người dùng. Không tìm thấy người dùng.',
        404,
      );
    }
  }
}
